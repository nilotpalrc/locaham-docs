.. LocAham Android documentation master file, created by
   sphinx-quickstart on Mon Apr 09 06:35:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction
===========================================
Welcome to LocAham Android application's documentation!

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   

Purpose
--------
The purpose of this document is to describe the LocAham Android application. This
document contains the functional, behavioural and non-functional requirements of this
project.

Scope
-----
The android application would basically provide the users with a one stop access to all
the camera feeds at their locations. The application would also detect unusual activities
in the scenes and notify the users using the notification system and save those moments
as events in a separate section of the application.

Operating Environment
---------------------
The application’s minimum SDK should be set at API 15 which is Android version 4.0.3
and this would allow about 100 % coverage of the available android devices.

Assumptions and Dependencies
----------------------------
This document assumes that the application is allowed the following android permissions:

- CAMERA
- INTERNET
- VIBRATE
- RECORD_AUDIO
- ACCESS_FINE_LOCATION
- CALL_PHONE
- SEND_SMS
- READ_EXTERNAL_STORAGE
- WRITE_EXTERNAL_STORAGE

System Features
===============
Functional Requirements
-----------------------
The functional requirements are :

1. Authentication : Can be implemented using Firebase Authentication for simplicity.
2. Historical Data Storage : Should store
3. Automatic Camera discovery
4. Support for Pan, Tilt and Zoom(PTZ)
5. ONVIF Support
6. Motion activation sensitivity
7. Easy switching from activity detected clip to the extended video.
8. Support for calling home or emergency services right from the application.
9. Should work on a Cellular connection.
10. Should have support for h264 streams.


.. Indices and tables
.. ==================
.. 
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`